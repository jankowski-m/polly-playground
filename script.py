#!/usr/bin/python

import boto3

class SpeechPart(object):
    client = boto3.client('polly')

    persona_to_voice_map =  {
        "Lys.": "Joey",
        "Mel.": "Emma",
        "Nic.": "Matthew",
        "La.": "Justin",
        "Soc.": "Brian",
        "narrator": "Amy" # also default
    }
    output_format = "mp3"
    text_type = "text"

    def __init__(self, text_line, persona="narrator"):
        self._text_line = text_line
        self._persona = persona

    @classmethod
    def choose_voice(cls, persona):
        default = cls.persona_to_voice_map["narrator"]
        return cls.persona_to_voice_map.get(persona, default)

    @classmethod
    def determine_persona(cls, text_line):
        """
        extract persona from the dialog line
        """
        for persona in cls.persona_to_voice_map.keys():
            if persona in text_line[:4]:
                return persona

    @staticmethod
    def remove_persona_from_text_line(persona, text_line):
        return text_line.lstrip(persona)

    def synthesize(self):
        """
        :return: binary audio stream
        """
        persona = SpeechPart.determine_persona(self._text_line)
        self._text_line = SpeechPart.remove_persona_from_text_line(persona, self._text_line)
        self._persona = persona

        response = self.client.synthesize_speech(
            OutputFormat=self.output_format,
            Text=self._text_line,
            TextType="text",
            VoiceId=self.choose_voice(persona)
        )
        if "AudioStream" not in response:
            raise KeyError # ?
        with closing(response["AudioStream"]) as stream:
            return stream.read()

def split_line_to_max_1500_char_chunks(text_line, split_on="."):
    lines = [text_line]
    line_length = len(text_line)
    if line_length > 1500:
        print(text_line[:30], line_length)
        persona = SpeechPart.determine_persona(text_line)
        text_line = SpeechPart.remove_persona_from_text_line(persona, text_line)
        lines = text_line.split(split_on)
        for l in lines:
            l = persona+l
    return lines

with open('laches.txt', 'r') as input_file:
    with open("laches.mp3", 'wb') as output_file:
        for part in input_file.readlines():
            try:
                text_lines =split_line_to_max_1500_char_chunks(part)
                for text_line in text_lines:
                    print(len(text_line))
                    output_file.write(SpeechPart(text_line=text_line).synthesize())
            except Exception as e:
                print(e)
